(function(){

  angular
    .module("GroceryApp")
    .config(GroceryAppConfig);
  GroceryAppConfig.$inject = ["$stateProvider","$urlRouterProvider"];

  function GroceryAppConfig($stateProvider,$urlRouterProvider){
    $stateProvider
      .state('Home', {
        url : '/Home',
        templateUrl: 'app/home/home.html'
      })
      .state('Search', {
        url : '/Search',
        templateUrl: 'app/search/search.html',
        controller : 'SearchCtrl',
        controllerAs : 'ctrl'
      })
      .state('Edit', {
          url: "/Edit/:productId",
          templateUrl: "app/edit/edit.html",
          controller : 'EditCtrl',
          controllerAs : 'ctrl'
      })
    $urlRouterProvider.otherwise("/Home");
  }

})();