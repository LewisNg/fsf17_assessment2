(function(){
    angular
        .module("GroceryApp")
        .controller("EditCtrl", EditCtrl);

    EditCtrl.$inject = ["$state","GroceryService"];

    function EditCtrl(state, GroceryService){
        var vm = this;
        vm.searchString = state.params.productId;
        vm.result = {};

        // expose all the functions to the view
        // vm.deleteManager = deleteManager;
        //vm.initDetails = initDetails;
        vm.updateBarcode = updateBarcode;
        vm.updateBrand = updateBrand;
        vm.updateName = updateName;
        vm.cancelBarcode = cancelBarcode;
        vm.cancelBrand = cancelBrand;
        vm.cancelName = cancelName;
        //vm.search = search;
        vm.toggleBarcodeEditor = toggleBarcodeEditor;
        vm.toggleBrandEditor = toggleBrandEditor;
        vm.toggleNameEditor = toggleNameEditor;

        initDetails();
        function initDetails(){
            GroceryService
                .retrieveProduct(state.params.productId)
                .then(function(result){
                    vm.result.barcode = result.data.upc12;    
                    vm.result.brand = result.data.brand;
                    vm.result.name = result.data.name;
                      
                }).catch(function(error){
                    console.log(JSON.stringify(error));
                })
        }

        // function deleteManager(){
        //     console.log("delete manager ..");
        //     DeptService
        //         .deleteDept(vm.dept_no, vm.result.manager_id)
        //         .then(function (response){
        //             search();
        //         })
        //         .catch(function (err){
        //             console.log(JSON.stringify(err));
        //         });
        // }


        // function initDetails(){
        //     vm.result.dept_no = "";
        //     vm.result.dept_name = "";
        //     vm.result.manager_id = "";
        //     vm.result.manager_name = "";
        //     vm.result.manager_from = "";
        //     vm.result.manager_to = "";
        //     vm.showDetails = false;
        //     vm.isEditorOn = false;
        // }

        function updateProduct(){
            console.log("Update product..");
            GroceryService
                .updateProduct(vm.searchString, vm.result.barcode, vm.result.brand, vm.result.name)
                .then(function(response){
                    console.log(JSON.stringify(response.data));
                }).catch(function(err){
                    console.log(err);
                });
        }

        function toggleBarcodeEditor(){
            if (!vm.isBarcodeEditorOn){
                vm.isNameEditorOn = false;
                vm.isBrandEditorOn = false;
            }            
            vm.isBarcodeEditorOn = !(vm.isBarcodeEditorOn);
        } 
        function toggleBrandEditor(){
            if (!vm.isBrandEditorOn){
                vm.isBarcodeEditorOn = false;
                vm.isNameEditorOn = false;
            }
            vm.isBrandEditorOn = !(vm.isBrandEditorOn);
        }
        function toggleNameEditor(){
            if (!vm.isNameEditorOn){
                vm.isBarcodeEditorOn = false;
                vm.isBrandEditorOn = false;
            }
            vm.isNameEditorOn = !(vm.isNameEditorOn);
        }

        function updateBarcode(){
            updateProduct();
            toggleBarcodeEditor();
        }
        function updateBrand(){
            updateProduct();
            toggleBrandEditor();
        }
        function updateName(){
            updateProduct();
            toggleNameEditor();
        }

        function cancelUpdate(){
            initDetails();
        }
        function cancelBarcode(){
            cancelUpdate();
            toggleBarcodeEditor();
        }
        function cancelBrand(){
            cancelUpdate();
            toggleBrandEditor();
        }
        function cancelName(){
            cancelUpdate();
            toggleNameEditor();
        }
        

    }

    

})();