(function(){
    angular
        .module("GroceryApp")
        .service("GroceryService", GroceryService);

    GroceryService.$inject = ['$http'];

    function GroceryService($http){
        var service = this;
        // service.insertDept = insertDept;
        service.retrieveGrocery = retrieveGrocery;
        service.retrieveProduct = retrieveProduct;
        // service.retrieveDeptManagerDB = retrieveDeptManagerDB;
        // service.deleteDept = deleteDept;
        service.updateProduct = updateProduct;
        // service.retrieveDeptByID = retrieveDeptByID;
        // service.retrieveDept = retrieveDept;

        // function insertDept(department){
        //     return $http({
        //         method: 'POST',
        //         url: "api/departments",
        //         data: {dept: department}
        //     });
        // }
        // function retrieveDept() {
        //     return $http({
        //         method: 'GET'
        //         , url: 'api/static/departments'
        //     });
        // }

        function retrieveGrocery(searchString){
            return $http({
                method: 'GET',
                url: "api/grocery",
                params: {'searchString': searchString}
            });
        }

        function retrieveProduct(searchString){
            return $http({
                method: 'GET',
                url: "api/product",
                params: {'searchString': searchString}
            });
        }
        // function retrieveDeptManagerDB(searchString){
        //     return $http({
        //         method: 'GET',
        //         url: "/api/departments/managers",
        //         params: {'searchString': searchString}
        //     });
        // }

        // function deleteDept(dept_no, emp_no){
        //     return $http({
        //         method: 'DELETE',
        //         url: "/api/departments/" + dept_no + "/manager/" + emp_no
        //     });
        // }

        function updateProduct(id, barcode, brand, name){
            return $http({
                method: 'PUT',
                url: "/api/product/" + id,
                data: {
                    barcode: barcode, 
                    brand: brand,
                    name: name
                }
            });
        }

        // function retrieveDeptByID(dept_no){
        //     console.log(dept_no);
        //     return $http({
        //         method: 'GET',
        //         url: "/api/departments/managers/?searchString=" + dept_no
        //     });
        // }
        

    }
})();