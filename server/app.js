var express = require("express");
var path = require("path");
var bodyParser = require("body-parser");
var Sequelize = require("sequelize");

const NODE_PORT = process.env.NODE_PORT || 8080;
const CLIENT_FOLDER = path.join(__dirname, '../client');
const MSG_FOLER = path.join(CLIENT_FOLDER, 'assets/messages');
const MYSQL_USERNAME = "root";
const MYSQL_PASSWORD = "mySQ1!sql";
const API_GROCERY_ENDPOINT = "/api/grocery";

var app = express();

var sequelize = new Sequelize(
    'grocery_list',
    MYSQL_USERNAME,
    MYSQL_PASSWORD,
    {
        host: 'localhost',
        logging: console.log,
        dialect: 'mysql',
        pool: {
            max: 5,
            min: 0,
            idle: 10000
        }   
    }
);

var Grocery = require('./models/grocery_list')(sequelize, Sequelize);

app.use(express.static(CLIENT_FOLDER));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

// app.post(API_GROCERY_ENDPOINT, function(req, res){
//     console.log("Inserting dept");
//     console.log(req.body);
//     Department
//         .create({
//             dept_no: req.body.dept.no,
//             dept_name: req.body.dept.name
//         }).then(function(department){
//             res.status(200).json(department);
//         }).catch(function(err){
//             res.status(500).json(err);
//         });
    
// });

app.get(API_GROCERY_ENDPOINT, function(req, res){
    Grocery
        .findAll({
            where: {
                $or: [
                    {brand: {$like: "%" + req.query.searchString + "%"}},
                    {name: {$like: "%" + req.query.searchString + "%"}}
                ]
            }
        }).then(function(result) {
            res 
                .status(200)
                .json(result);
        }).catch(function(err){
            res
                .status(500)
                .json(err);
        })
});

app.get("/api/product", function(req, res){
    console.log(req.query.searchString);
    Grocery
        .findOne({
            where: {
                id: req.query.searchString
            }
        }).then(function(result) {
            res 
                .status(200)
                .json(result);
        }).catch(function(err){
            res
                .status(500)
                .json(err);
        })
});

app.put("/api/product/:id", function(req,res){
    var where = {};
    where.id = req.params.id;
    //where.dept_name = req.params.dept_name;
    Grocery
        .update({upc12: req.body.barcode
                , brand: req.body.brand
                , name: req.body.name},
            {where: where}
        ).then(function(result) {
            res 
                .status(200)
                .json(result);
        }).catch(function(err){
            console.log(err);
            res
                .status(500)
                .json(err);
        })
});

// app.delete(API_GROCERY_ENDPOINT + "/:dept_no/manager/:emp_no", function(req, res){
//     console.log("delete dept");
//     console.log(req.params.dept_no);
//     var whereClause = {};
//     whereClause.dept_no = req.params.dept_no;
//     whereClause.emp_no = req.params.emp_no;
    
//     DeptManager
//         .destroy({
//             where: whereClause
//         }).then(function (result){
//             console.log(result);
//             if(result > 0){
//                 res.status(200).json({success: true});
//             }else{
//                 res.status(200).json({success: false});
//             }
//         }).catch(function(err){
//             res.status(500).json(err);
//         });
// });


// app.get("/api/departments/:dept_no", function (req, res) {
//     console.log
//     var where = {};
//     if (req.params.dept_no) {
//         where.dept_no = req.params.dept_no
//     }

//     console.log("where " + where);
//     // We use findOne because we know (by looking at the database schema) that dept_no is the primary key and
//     // is therefore unique. We cannot use findById because findById does not support eager loading
//     Department
//         .findOne({
//             where: where
//             , include: [{
//                 model: Manager
//                 , order: [["to_date", "DESC"]]
//                 , limit: 1
//                 , include: [Employee]
//             }]
//         })
//         .then(function (departments) {
//             console.log("-- GET /api/departments/:dept_no findOne then() result \n " + JSON.stringify(departments));
//             res.json(departments);
//         })
//         // this .catch() handles erroneous findAll operation
//         .catch(function (err) {
//             console.log("-- GET /api/departments/:dept_no findOne catch() \n " + JSON.stringify(departments));
//             res
//                 .status(500)
//                 .json({error: true});
//         });
// });

// app.get("/api/static/departments", function(req,res) {
//     var departments = [
//         {
//             deptNo: 1001,
//             deptName: "Admin"
//         },
//         {
//             deptNo: 1002,
//             deptName: "Finance"
//         },
//         {
//             deptNo: 1003,
//             deptName: "Sales"
//         },
//         {
//             deptNo: 1004,
//             deptName: "HR"
//         },
//         {
//             deptNo: 1005,
//             deptName: "Staff"
//         },
//         {
//             deptNo: 1006,
//             deptName: "Customer Care"
//         },
//         {
//             deptNo: 1007,
//             deptName: "Support"
//         }
//     ];
//     res.status(200).json(departments);
// });

app.use(function(req,res) {
    res.status(400).sendFile(path.join(MSG_FOLER, "404.html"));
});

app.use(function(err,req, res, next){
    res.status(500).sendFile(path.join(MSG_FOLER, "500.html"));
});

app.listen(NODE_PORT, function() {
    console.log("Server running at port " + NODE_PORT);
})