module.exports = function(conn, Sequelize) {
    var Grocery = conn.define("grocery_list", {
        id: {
            type: Sequelize.INTEGER(11),
            allowNull: false,
            primaryKey: true,
            autoIncrement: true
        },
        upc12: {
            type: Sequelize.BIGINT(12),
            allowNull:false
        },
        brand: {
            type: Sequelize.STRING,
            allowNull:false
        },
        name: {
            type: Sequelize.STRING,
            allowNull:false
        }

    },{
        freezeTableName: true,
        timestamps: false
    });
    return Grocery;
};